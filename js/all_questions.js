var json = 
[{
"question": "Name a board game that people play on Christmas Eve", 
"answers": ["Monopoly", "Scrabble", "Risk", "Trivial Pursuit", "Pictionary", "Clue", "Chess", "Checkers"],
"points": [30, 20, 15, 10, 9, 8, 5, 3]
},
{
"question": "Name something people have been happy to have during the pandemic", 
"answers": ["Internet", "Phone", "Netflix", "Family", "Games", "Food", "Alcohol", "Spouse"],
"points": [47, 21, 10, 8, 6, 4, 3, 1]
},
{
  "question": "Name a popular non-alcoholic winter beverage", 
  "answers": ["Egg Nog", "Hot Chocolate", "Apple Cider", "Coffee", "Tea", "Chai", "Milk"],
  "points": [42, 16, 12, 10, 10, 8, 2]
},
{
  "question": "Name something people might start doing on December 1st", 
  "answers": ["Listen to Christmas music", "Christmas Shopping", "Decorating the Tree", "Putting up Lights", "Sending Cards", "Baking Cookies"],
  "points": [40, 23, 16, 12, 7, 2]
},
{
  "question": "Name a popular Christmas movie", 
  "answers": ["Elf", "Home Alone", "Christmas Story", "Die Hard", "Grinch", "Charlie Brown", "Christmas Vacation"],
  "points": [30, 22, 18, 13, 10, 4, 3]
},
{
  "question": "Name something associated with Santa Claus", 
  "answers": ["Reindeer", "Mrs. Claus", "Sleigh", "Beard", "Hat", "Gifts", "Cookies", "Belly"],
  "points": [30, 16, 13, 9, 9, 9, 8, 6]
},
{
  "question": "Name a family Christmas tradition", 
  "answers": ["Advent Calendar", "Elf on the Shelf", "New Ornament", "Bickering", "Letter to Santa", "Gingerbread House", "Christmas Movie", "Visit Santa"],
  "points": [23, 22, 16, 12, 11, 9, 4, 3]
},
{
  "question": "Name a popular Christmas carol", 
  "answers": ["Silent Night", "Jingle Bells", "Deck the Halls", "Joy to the World", "The First Noel", "Little Drummer Boy", "We Wish You a Merry Xmas", "Rudolph"],
  "points": [22, 20, 17, 16, 14, 5, 3, 3]
},
{
  "question": "Name a common cause of a Christmas injury", 
  "answers": ["Run over by Reindeer", "Crushed by Tree", "Burned on Stove", "Stepped on an Ornament", "Bad gift to Spouse", "Skied into Tree", "BB Gun"],
  "points": [25, 20, 17, 16, 14, 5, 3]
},
{
  "question": "Name the worst gift you could give your partner", 
  "answers": ["Socks", "Divorce", "Nothing", "Vacuum", "Coal", "Gym Membership", "Gift Card", "Kids"],
  "points": [18, 18, 17, 16, 11, 9, 6, 5]
},
{
  "question": "Name something you wanted from Santa as a child", 
  "answers": ["Bike", "Doll", "Video Games", "Revenge", "Barbie", "Action Figure", "Puppy", "Rollerskates"],
  "points": [27, 19, 18, 11, 9, 7, 5, 4]
},
{
  "question": "Name a festive drink that people would have at a Christmas party", 
  "answers": ["Mulled Wine", "Wine", "Rum + Eggnog", "Baileys", "Caesar", "Bubbly", "Rum on the Rocks", "Punch"],
  "points": [26, 19, 17, 11, 10, 7, 6, 4]
},
{
  "question": "Name a food often found at Christmas Dinner", 
  "answers": ["Turkey", "Potatoes", "Ham", "Cranberry Sauce", "Dinner Rolls", "Brussels", "Stuffing", "Pie"],
  "points": [46, 29, 11, 3, 3, 3, 3, 2]
},
{
  "question": "Name something people do on Boxing Day", 
  "answers": ["Sleep/Relax", "Shop/Find Sales", "Return Gifts", "Eat", "Watch TV", "Clean", "Play with Presents", "Go for a Walk"],
  "points": [31, 22, 11, 10, 8, 7, 6, 5]
},
{
  "question": "Name a winter activity that makes you sweat", 
  "answers": ["Skiing", "Ice Skating", "Hockey", "Shovelling", "Sledding", "Snowball Fight"],
  "points": [29, 19, 16, 14, 14, 8]
},
{
  "question": "Name a word or phrase that begins with “snow”", 
  "answers": ["Snowman", "Snowball", "Snowflake", "Snow White", "Snow Angel", "Snow Day", "Snow Globe", "Snowing"],
  "points": [24, 23, 22, 12, 7, 6, 4, 2]
},



]
